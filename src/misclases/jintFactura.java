/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misclases;

import java.awt.Color;
import javax.swing.JOptionPane;

/**
 *
 * @author _Windows_
 */
public class jintFactura extends javax.swing.JInternalFrame {

    /**
     * Creates new form JintCotizacion
     */
    public jintFactura() {
        initComponents();
        this.desahabilitar();
        this.resize(1200,900);
        getContentPane().setBackground(new Color(34, 34, 34));

    }
    public void limpiar(){
        this.txtNumRFC.setText("");
        this.txtNumFactura.setText("");
        this.txtDomicilio.setText("");
        this.txtNombreCliente.setText("");
        this.txtDescripcion.setText("");
        this.txtTotalVenta.setText("");
        this.txtFecha.setText("");
        this.txtTotalVentaF.setText("");
        this.txtTotalPagarF.setText("");
        this.txtImpuestoF.setText("");
    }
    public void desahabilitar(){
        //Desabilitar ELementos
        this.txtNumRFC.setEnabled(false);
        this.txtNumFactura.setEnabled(false);
        this.txtDomicilio.setEnabled(false);
        this.txtNombreCliente.setEnabled(false);
        this.txtDescripcion.setEnabled(false);
        this.txtTotalVenta.setEnabled(false);
        this.txtFecha.setEnabled(false);
        this.txtTotalVentaF.setEnabled(false);
        this.txtTotalPagarF.setEnabled(false);
        this.txtImpuestoF.setEnabled(false);
    
        
        //Butoms
        this.btnMostar.setEnabled(false);
        this.btnGuardar.setEnabled(false);
    
    }

    
    public void habilitar(){
    
         //Desabilitar ELementos
        this.txtNumRFC.setEnabled(true);
        this.txtNumFactura.setEnabled(true);
        this.txtDomicilio.setEnabled(true);
        this.txtNombreCliente.setEnabled(true);
        this.txtDescripcion.setEnabled(true);
        this.txtFecha.setEnabled(true);
        this.txtTotalVenta.setEnabled(true);
    
        
        //Butoms
        this.btnGuardar.setEnabled(true);
    
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        bindingGroup = new org.jdesktop.beansbinding.BindingGroup();

        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtNumFactura = new javax.swing.JTextField();
        txtNumRFC = new javax.swing.JTextField();
        txtNombreCliente = new javax.swing.JTextField();
        btnCerrar = new javax.swing.JButton();
        btnMostar = new javax.swing.JButton();
        btnNuevo = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        pamFactura = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtTotalPagarF = new javax.swing.JTextField();
        txtTotalVentaF = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        txtImpuestoF = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        txtDomicilio = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        txtDescripcion = new javax.swing.JTextField();
        txtFecha = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        txtTotalVenta = new javax.swing.JTextField();

        setBackground(new java.awt.Color(39, 39, 39));
        setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        setClosable(true);
        setForeground(new java.awt.Color(51, 51, 51));
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        getContentPane().setLayout(null);

        jLabel2.setBackground(new java.awt.Color(255, 255, 255));
        jLabel2.setFont(new java.awt.Font("Times New Roman", 0, 48)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Factura");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(520, 0, 220, 90);

        jLabel3.setBackground(new java.awt.Color(255, 255, 255));
        jLabel3.setFont(new java.awt.Font("Times New Roman", 0, 36)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Numero de Factura");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(190, 60, 410, 90);

        jLabel4.setBackground(new java.awt.Color(255, 255, 255));
        jLabel4.setFont(new java.awt.Font("Times New Roman", 0, 36)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("RFC");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(390, 130, 150, 90);

        jLabel5.setBackground(new java.awt.Color(255, 255, 255));
        jLabel5.setFont(new java.awt.Font("Times New Roman", 0, 36)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Nombre del Cliente");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(180, 180, 290, 90);

        jLabel6.setBackground(new java.awt.Color(255, 255, 255));
        jLabel6.setFont(new java.awt.Font("Times New Roman", 0, 36)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Domicilio Fiscal");
        getContentPane().add(jLabel6);
        jLabel6.setBounds(220, 240, 390, 90);

        txtNumFactura.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        txtNumFactura.setToolTipText("");

        org.jdesktop.beansbinding.Binding binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, txtNumRFC, org.jdesktop.beansbinding.ObjectProperty.create(), txtNumFactura, org.jdesktop.beansbinding.BeanProperty.create("nextFocusableComponent"));
        bindingGroup.addBinding(binding);

        getContentPane().add(txtNumFactura);
        txtNumFactura.setBounds(490, 90, 390, 40);

        txtNumRFC.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        txtNumRFC.setToolTipText("");
        getContentPane().add(txtNumRFC);
        txtNumRFC.setBounds(490, 150, 390, 40);

        txtNombreCliente.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        txtNombreCliente.setToolTipText("");

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, txtDomicilio, org.jdesktop.beansbinding.ObjectProperty.create(), txtNombreCliente, org.jdesktop.beansbinding.BeanProperty.create("nextFocusableComponent"));
        bindingGroup.addBinding(binding);

        txtNombreCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNombreClienteActionPerformed(evt);
            }
        });
        getContentPane().add(txtNombreCliente);
        txtNombreCliente.setBounds(490, 210, 390, 40);

        btnCerrar.setFont(new java.awt.Font("Tw Cen MT", 0, 36)); // NOI18N
        btnCerrar.setText("Cerrar");
        btnCerrar.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        btnCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarActionPerformed(evt);
            }
        });
        getContentPane().add(btnCerrar);
        btnCerrar.setBounds(980, 520, 200, 70);

        btnMostar.setFont(new java.awt.Font("Tw Cen MT", 0, 36)); // NOI18N
        btnMostar.setText("Mostrar");
        btnMostar.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        btnMostar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMostarActionPerformed(evt);
            }
        });
        getContentPane().add(btnMostar);
        btnMostar.setBounds(990, 400, 180, 70);

        btnNuevo.setFont(new java.awt.Font("Tw Cen MT", 0, 36)); // NOI18N
        btnNuevo.setText("Nuevo");
        btnNuevo.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });
        getContentPane().add(btnNuevo);
        btnNuevo.setBounds(990, 110, 180, 70);

        btnGuardar.setFont(new java.awt.Font("Tw Cen MT", 0, 36)); // NOI18N
        btnGuardar.setText("Guardar");
        btnGuardar.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });
        getContentPane().add(btnGuardar);
        btnGuardar.setBounds(990, 250, 180, 70);

        btnLimpiar.setFont(new java.awt.Font("Tw Cen MT", 0, 36)); // NOI18N
        btnLimpiar.setText("Limpiar");
        btnLimpiar.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });
        getContentPane().add(btnLimpiar);
        btnLimpiar.setBounds(980, 610, 200, 70);

        btnCancelar.setFont(new java.awt.Font("Tw Cen MT", 0, 36)); // NOI18N
        btnCancelar.setText("Cancelar");
        btnCancelar.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        getContentPane().add(btnCancelar);
        btnCancelar.setBounds(980, 700, 200, 70);

        pamFactura.setBackground(new java.awt.Color(153, 153, 153));
        pamFactura.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Factura", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Trebuchet MS", 0, 36))); // NOI18N
        pamFactura.setForeground(java.awt.Color.gray);
        pamFactura.setFont(new java.awt.Font("Trebuchet MS", 0, 18)); // NOI18N
        pamFactura.setLayout(null);

        jLabel1.setBackground(new java.awt.Color(255, 255, 255));
        jLabel1.setFont(new java.awt.Font("Times New Roman", 0, 36)); // NOI18N
        jLabel1.setText("Total a Pagar");
        pamFactura.add(jLabel1);
        jLabel1.setBounds(90, 190, 340, 60);

        txtTotalPagarF.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        txtTotalPagarF.setToolTipText("");
        pamFactura.add(txtTotalPagarF);
        txtTotalPagarF.setBounds(320, 210, 390, 40);

        txtTotalVentaF.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        txtTotalVentaF.setToolTipText("");
        txtTotalVentaF.setFocusTraversalPolicyProvider(true);
        pamFactura.add(txtTotalVentaF);
        txtTotalVentaF.setBounds(320, 60, 390, 40);

        jLabel16.setBackground(new java.awt.Color(255, 255, 255));
        jLabel16.setFont(new java.awt.Font("Times New Roman", 0, 36)); // NOI18N
        jLabel16.setText("Total de Venta");
        pamFactura.add(jLabel16);
        jLabel16.setBounds(70, 50, 360, 60);

        txtImpuestoF.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        txtImpuestoF.setToolTipText("");
        pamFactura.add(txtImpuestoF);
        txtImpuestoF.setBounds(320, 130, 390, 40);

        jLabel17.setBackground(new java.awt.Color(255, 255, 255));
        jLabel17.setFont(new java.awt.Font("Times New Roman", 0, 36)); // NOI18N
        jLabel17.setText("Impuesto");
        pamFactura.add(jLabel17);
        jLabel17.setBounds(150, 120, 280, 60);

        getContentPane().add(pamFactura);
        pamFactura.setBounds(10, 500, 960, 300);

        txtDomicilio.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        txtDomicilio.setToolTipText("");
        getContentPane().add(txtDomicilio);
        txtDomicilio.setBounds(490, 270, 390, 40);

        jLabel11.setBackground(new java.awt.Color(255, 255, 255));
        jLabel11.setFont(new java.awt.Font("Times New Roman", 0, 36)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Descripcion");
        getContentPane().add(jLabel11);
        jLabel11.setBounds(280, 300, 190, 90);

        jLabel12.setBackground(new java.awt.Color(255, 255, 255));
        jLabel12.setFont(new java.awt.Font("Times New Roman", 0, 36)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("Fecha de Venta");
        getContentPane().add(jLabel12);
        jLabel12.setBounds(230, 360, 380, 90);

        txtDescripcion.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        txtDescripcion.setToolTipText("");
        txtDescripcion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDescripcionActionPerformed(evt);
            }
        });
        getContentPane().add(txtDescripcion);
        txtDescripcion.setBounds(490, 330, 390, 40);

        txtFecha.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        txtFecha.setToolTipText("");
        getContentPane().add(txtFecha);
        txtFecha.setBounds(490, 390, 390, 40);

        jLabel13.setBackground(new java.awt.Color(255, 255, 255));
        jLabel13.setFont(new java.awt.Font("Times New Roman", 0, 36)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText("Total de Venta");
        getContentPane().add(jLabel13);
        jLabel13.setBounds(240, 420, 370, 90);

        txtTotalVenta.setFont(new java.awt.Font("Tahoma", 0, 36)); // NOI18N
        txtTotalVenta.setToolTipText("");
        txtTotalVenta.setFocusTraversalPolicy(null);
        txtTotalVenta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTotalVentaActionPerformed(evt);
            }
        });
        getContentPane().add(txtTotalVenta);
        txtTotalVenta.setBounds(490, 450, 390, 40);

        bindingGroup.bind();

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtNombreClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNombreClienteActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNombreClienteActionPerformed

    private void btnCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarActionPerformed
        int opcion = JOptionPane.showConfirmDialog(this, "Qieres Cerrar el Programa?","Factura",JOptionPane.YES_NO_OPTION);

                if(opcion == JOptionPane.YES_OPTION){
                    this.dispose();
                }

        // TODO add your handling code here:
    }//GEN-LAST:event_btnCerrarActionPerformed

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        this.habilitar();
        fac = new Factura();
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void txtDescripcionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDescripcionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDescripcionActionPerformed

    private void txtTotalVentaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTotalVentaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTotalVentaActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed

        this.limpiar();
        this.desahabilitar();

        // TODO add your handling code here:
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
        this.limpiar();
        // TODO add your handling code here:
    }//GEN-LAST:event_btnLimpiarActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        boolean exito = false;
        if(this.txtNumFactura.getText().equals(""))exito = true;
        if(this.txtNombreCliente.getText().equals(""))exito = true;
        if(this.txtDescripcion.getText().equals(""))exito = true;
        if(this.txtDomicilio.getText().equals(""))exito = true;
        if(this.txtFecha.getText().equals(""))exito = true;
        if(this.txtNumRFC.getText().equals(""))exito = true;
        if(this.txtTotalVenta.getText().equals(""))exito = true;
        
        if(exito==true){
            //Falto informacion
            JOptionPane.showMessageDialog(this,"Falto Capturar Información");
        }
        else{
            /*cot.setNumCotizacion(Integer.parseInt(this.txtNumero.getText()));  //Convertir String A Entero
            cot.setDescripcion(this.txtDescripcion.getText());*/
            //Todo esta correcto
            fac.setTotalVenta(Float.parseFloat(this.txtTotalVenta.getText()));
            fac.setNumFactura(Integer.parseInt(this.txtNumFactura.getText()));
            fac.setNombreRFC(this.txtNumRFC.getText());
            fac.setNombreCliente(this.txtNombreCliente.getText());
            fac.setDomicilioFiscal(this.txtDomicilio.getText());
            fac.setDescripcion(this.txtDescripcion.getText());
            fac.setFechaVenta(this.txtFecha.getText());
            JOptionPane.showMessageDialog(this,"Se Guardo Correctamente la Información");
            this.btnMostar.setEnabled(true);
        }

        // TODO add your handling code here:
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnMostarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMostarActionPerformed
        this.txtNumFactura.setText(String.valueOf(fac.getNumFactura()));
        this.txtNombreCliente.setText(fac.getNombreCliente());
        this.txtDescripcion.setText(fac.getDescripcion());
        this.txtDomicilio.setText(fac.getDomicilioFiscal());
        this.txtNumRFC.setText(fac.getNombreRFC());
        this.txtFecha.setText(fac.getFechaVenta());
        this.txtTotalVenta.setText(String.valueOf(fac.getTotalVenta()));
        
        
        //Mostrar los resultados
        
        this.txtTotalVentaF.setText(String.valueOf(fac.calcularTotalVenta()));
        this.txtImpuestoF.setText(String.valueOf(fac.calcularImpuesto()));
        this.txtTotalPagarF.setText(String.valueOf(fac.calcularTotalPagar()));

        // TODO add your handling code here:
    }//GEN-LAST:event_btnMostarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnCerrar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JButton btnMostar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel pamFactura;
    private javax.swing.JTextField txtDescripcion;
    private javax.swing.JTextField txtDomicilio;
    private javax.swing.JTextField txtFecha;
    private javax.swing.JTextField txtImpuestoF;
    private javax.swing.JTextField txtNombreCliente;
    private javax.swing.JTextField txtNumFactura;
    private javax.swing.JTextField txtNumRFC;
    private javax.swing.JTextField txtTotalPagarF;
    private javax.swing.JTextField txtTotalVenta;
    private javax.swing.JTextField txtTotalVentaF;
    private org.jdesktop.beansbinding.BindingGroup bindingGroup;
    // End of variables declaration//GEN-END:variables

    
    
    private Factura fac;

}
