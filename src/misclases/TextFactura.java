/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misclases;

//Importar Clases
import java.util.Scanner;   //Escanea datos de entrada por cmd (Genera el objeto)

/**
 *
 * @author _Windows_
 */
public class TextFactura {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        /*Terreno terreno = new Terreno();
        terreno.setAncho(35.5f);
        terreno.setLargo(100.0f);
        terreno.imprimirTerreno();
        
        Terreno ter = new Terreno(10.0f,90.0f);
        ter.imprimirTerreno();
        
        
        Terreno ter2 = new Terreno(terreno);
        ter2.imprimirTerreno();  */

        Factura factura = new Factura();
        Scanner sc = new Scanner(System.in);
        int opcion = 0, numFactura = 0;
        String nombreRFC = "", nombreCliente = "", domicilioFiscal = "", descripcion = "", fechaVenta = "";
        float totalVenta = 0.0f;

        do {
            //Pintar el menu
            System.out.println("1-Iniciar el Objeto");
            System.out.println("2-Cambiar numFactura");
            System.out.println("3-Cambiar nombreRFC");
            System.out.println("4-Cambiar nombreCliente");
            System.out.println("5-Cambiar domicilioFiscal");
            System.out.println("6-Cambiar descripcion");
            System.out.println("7-Cambiar fechaVenta");
            System.out.println("8-Cambiar totalVenta");
            System.out.println("9-Mostrar ");
            System.out.println("10-Salir");
            System.out.println("Dame la opcion: ");
            opcion = sc.nextInt();

            switch (opcion) {
                case 1:
                    System.out.println("Teclea el numero de Factura");
                    numFactura = sc.nextInt();
                    sc.nextLine(); // Limpiar el buffer
                    System.out.println("Teclea el nombre del RFC");
                    nombreRFC = sc.nextLine();
                    System.out.println("Teclea el nombre del Cliente");
                    nombreCliente = sc.nextLine();
                    System.out.println("Teclea el domicilio fiscal");
                    domicilioFiscal = sc.nextLine();
                    System.out.println("Teclea la descripcion");
                    descripcion = sc.nextLine();
                    System.out.println("Teclea la fecha de venta");
                    fechaVenta = sc.nextLine();
                    System.out.println("Teclea el total de venta");
                    totalVenta = sc.nextFloat();

                    // Establecer los valores en el objeto Factura
                    factura.setNumFactura(numFactura);
                    factura.setNombreRFC(nombreRFC);
                    factura.setNombreCliente(nombreCliente);
                    factura.setDomicilioFiscal(domicilioFiscal);
                    factura.setDescripcion(descripcion);
                    factura.setFechaVenta(fechaVenta);
                    factura.setTotalVenta(totalVenta);

                    break;
                case 2:
                    System.out.println("Teclea el numero de Factura");
                    numFactura = sc.nextInt();
                    factura.setNumFactura(numFactura);
                    break;
                case 3:
                    sc.nextLine(); // Limpiar el buffer
                    System.out.println("Teclea el nuevo nombre del RFC");
                    nombreRFC = sc.nextLine();
                    factura.setNombreRFC(nombreRFC);
                    break;
                case 4:
                    sc.nextLine(); // Limpiar el buffer
                    System.out.println("Teclea el nuevo nombre del Cliente");
                    nombreCliente = sc.nextLine();
                    factura.setNombreCliente(nombreCliente);
                    break;
                case 5:
                    sc.nextLine(); // Limpiar el buffer
                    System.out.println("Teclea el nuevo domicilio fiscal");
                    domicilioFiscal = sc.nextLine();
                    factura.setDomicilioFiscal(domicilioFiscal);
                    break;
                case 6:
                    sc.nextLine(); // Limpiar el buffer
                    System.out.println("Teclea la nueva descripcion");
                    descripcion = sc.nextLine();
                    factura.setDescripcion(descripcion);
                    break;
                case 7:
                    sc.nextLine(); // Limpiar el buffer
                    System.out.println("Teclea la nueva fecha de venta");
                    fechaVenta = sc.nextLine();
                    factura.setFechaVenta(fechaVenta);
                    break;
                case 8:
                    System.out.println("Teclea el nuevo total de venta");
                    totalVenta = sc.nextFloat();
                    factura.setTotalVenta(totalVenta);
                    break;
                case 9:
                    // Mostrar los detalles de la factura
                    factura.imprimirContrato();
                    break;
                case 10:
                    System.out.println("Hasta la vista baby...!!");
                    break;
                default:
                    System.out.println("No es una opcion valida...!!!");
            }

        } while (opcion != 10);
    }
}