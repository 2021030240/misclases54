/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misclases;

/**
 *
 * @author _Windows_
 */
public class Factura {

    private int numFactura;
    private String nombreRFC;
    private String nombreCliente;
    private String domicilioFiscal;
    private String descripcion;
    private String fechaVenta;
    private float totalVenta;

    // Constructores
    public Factura() {
        this.numFactura = 0;
        this.nombreRFC = "";
        this.nombreCliente = "";
        this.domicilioFiscal = "";
        this.descripcion = "";
        this.fechaVenta = "";
        this.totalVenta = 0.0f;
    }

    public Factura(int numFactura, String nombreRFC, String nombreCliente, String domicilioFiscal,
                   String descripcion, String fechaVenta, float totalVenta) {
        this.numFactura = numFactura;
        this.nombreRFC = nombreRFC;
        this.nombreCliente = nombreCliente;
        this.domicilioFiscal = domicilioFiscal;
        this.descripcion = descripcion;
        this.fechaVenta = fechaVenta;
        this.totalVenta = totalVenta;
    }

    public Factura(Factura otraFactura) {
        this.numFactura = otraFactura.numFactura;
        this.nombreRFC = otraFactura.nombreRFC;
        this.nombreCliente = otraFactura.nombreCliente;
        this.domicilioFiscal = otraFactura.domicilioFiscal;
        this.descripcion = otraFactura.descripcion;
        this.fechaVenta = otraFactura.fechaVenta;
        this.totalVenta = otraFactura.totalVenta;
    }

    // Getters y Setters
    public int getNumFactura() {
        return numFactura;
    }

    public void setNumFactura(int numFactura) {
        this.numFactura = numFactura;
    }

    public String getNombreRFC() {
        return nombreRFC;
    }

    public void setNombreRFC(String nombreRFC) {
        this.nombreRFC = nombreRFC;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getDomicilioFiscal() {
        return domicilioFiscal;
    }

    public void setDomicilioFiscal(String domicilioFiscal) {
        this.domicilioFiscal = domicilioFiscal;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFechaVenta() {
        return fechaVenta;
    }

    public void setFechaVenta(String fechaVenta) {
        this.fechaVenta = fechaVenta;
    }

    public float getTotalVenta() {
        return totalVenta;
    }

    public void setTotalVenta(float totalVenta) {
        this.totalVenta = totalVenta;
    }

    // Métodos de comportamiento
    public float calcularTotalVenta() {
        return totalVenta;
    }

    public float calcularImpuesto() {
        // Supongamos un impuesto del 16% sobre la venta
        return totalVenta * 0.16f;
    }

    public float calcularTotalPagar() {
        return totalVenta + calcularImpuesto();
    }

    public void imprimirContrato() {
        System.out.println("Número de Factura: " + this.getNumFactura());
        System.out.println("Nombre/RFC: " + this.getNombreRFC());
        System.out.println("Cliente: " + this.getNombreCliente());
        System.out.println("Domicilio Fiscal: " + this.getDomicilioFiscal());
        System.out.println("Descripción: " + this.getDescripcion());
        System.out.println("Fecha de Venta: " + this.getFechaVenta());
        System.out.println("Total de Venta: " + this.getTotalVenta());
        System.out.println("Impuesto: " + this.calcularImpuesto());
        System.out.println("Total a Pagar: " + this.calcularTotalPagar());
        
        System.out.println("Presiona Enter para continuar...");
        new java.util.Scanner(System.in).nextLine();
    }
}